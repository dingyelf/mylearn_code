# mylearn_code

## 介绍

此仓库配合mylearn_note，主要存放学习过程的代码

## 代码目录

### nacos-discovery

nacos-discovery是简单的在本地实现了nacos服务注册、服务发现的demo，初学者可以通过安装nacos，了解到nacos提供的服务发现、服务注册、负载均衡等特性。
