package com.example.nacos.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRestProviderBootStrap {

    public static void main(String[] args) {
        SpringApplication.run(SpringRestProviderBootStrap.class,args);
    }

}
