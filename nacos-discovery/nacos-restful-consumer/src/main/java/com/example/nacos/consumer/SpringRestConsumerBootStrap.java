package com.example.nacos.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRestConsumerBootStrap {

    public static void main(String[] args) {
        SpringApplication.run(SpringRestConsumerBootStrap.class,args);
    }

}
